// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// AUTOGENERATED FILE
//
// This file is autogenerated! If you need to modify it, be sure to
// modify the script that exports patts voice data for use in Chrome.

// Initialize the voice array if it doesn't exist so that voice data files
// can be loaded in any order.

if (!window.voices) {
  window.voices = [];
}

// Add this voice to the global voice array.
window.voices.push({
  'projectFile': '/voice_data_hmm_en-GB_2/project',
  'prefix': '',
  'method': 'hmm',
  'cacheToDisk': false,
  'lang': 'en-GB',
  'gender': 'female',
  'removePaths': [],
  'files': [
    {
      'path': '/voice_data_hmm_en-GB_2/compile_hmm_22050_ph_lsp_swop_ap_msd.cfg',
      'url': '',
      'md5sum': '1808a35af7480b64cff4f1b67e568275',
      'size': 9381,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/engine_hmm_22050_ap-embedded_lsp.cfg',
      'url': '',
      'md5sum': '020c3128e7d289c7a4f153178d1be5dd',
      'size': 4766,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/g2p_m3_syls0_stress0_en-GB.fst',
      'url': '',
      'md5sum': '92d0550accb254f4feaed2678a620552',
      'size': 343388,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/hmm_voice_en_gb_fis_medium_22050_ph_lsp_swop_ap_msd_bin_8bit.voice',
      'url': '',
      'md5sum': 'f22d612c6ba34e2130be37d5fa8c564c',
      'size': 249690,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/compressed_lexicon_en_gb.blex',
      'url': '',
      'md5sum': '11890fb75031d2de6babc6775b34a403',
      'size': 1130256,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/textnorm_kestrel.cfg',
      'url': '',
      'md5sum': '97427f37e374112ac84c311c866c33b9',
      'size': 564,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/phonology.cfg',
      'url': '',
      'md5sum': '6491d3dbf027682bdab192f1b73d5509',
      'size': 12274,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/phonology.pb',
      'url': '',
      'md5sum': '527de500d8e45420d0635a9784ac5fd9',
      'size': 2226,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/resolve_homographs_simple_gb.pb',
      'url': '',
      'md5sum': 'f81138fb3f2ecb593c4ced1fb8dc4c95',
      'size': 42335,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/rewrite_android.far',
      'url': '',
      'md5sum': '4d9ee2adaf06943b409e786c270183bf',
      'size': 221720,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/roman_numeral_contexts.pb',
      'url': '',
      'md5sum': '6abaff02bd0b4ef77bdf3bdf13aafa36',
      'size': 8673,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/roman_numeral_android.far',
      'url': '',
      'md5sum': '8a7abc21cfd9cb207ce3133c0e4ede59',
      'size': 1282,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/textnorm_params.pb',
      'url': '',
      'md5sum': '385a978f1e72b289f81caea6d6c197bb',
      'size': 102,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/tokenize_and_classify_android.far',
      'url': '',
      'md5sum': '1c7ba93da95b5fa07822ec00ea973fd7',
      'size': 4047191,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/verbalize_android.far',
      'url': '',
      'md5sum': 'c3ac38d1da5b5c0babf1468cad1a7b7b',
      'size': 341259,
    },
    {
      'path': '/voice_data_hmm_en-GB_2/project',
      'url': '',
      'md5sum': 'c1b5d2c67ed413796da43f4b272c8439',
      'size': 1675,
    },
  ],
});
