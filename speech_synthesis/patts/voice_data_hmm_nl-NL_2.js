// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// AUTOGENERATED FILE
//
// This file is autogenerated! If you need to modify it, be sure to
// modify the script that exports patts voice data for use in Chrome.

// Initialize the voice array if it doesn't exist so that voice data files
// can be loaded in any order.

if (!window.voices) {
  window.voices = [];
}

// Add this voice to the global voice array.
window.voices.push({
  'projectFile': '/voice_data_hmm_nl-NL_2/project',
  'prefix': '',
  'method': 'hmm',
  'cacheToDisk': false,
  'lang': 'nl-NL',
  'gender': 'female',
  'removePaths': [],
  'files': [
    {
      'path': '/voice_data_hmm_nl-NL_2/compile_hmm_22050_ph_lsp_swop_ap_msd.cfg',
      'url': '',
      'md5sum': '1808a35af7480b64cff4f1b67e568275',
      'size': 9381,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/engine_hmm_22050_ap-embedded_lsp.cfg',
      'url': '',
      'md5sum': '020c3128e7d289c7a4f153178d1be5dd',
      'size': 4766,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/g2p_m3_syls0_stress0_nl-NL.fst',
      'url': '',
      'md5sum': '8b1a4252cf6c84cdad7b081fe98ac802',
      'size': 535879,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/hmm_voice_nl_nl_tfb_medium_22050_ph_lsp_swop_ap_msd_bin_8bit.voice',
      'url': '',
      'md5sum': 'dc36663dadf990f90f8625d5ba2eedcc',
      'size': 243934,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/compressed_lexicon_nl_nl.blex',
      'url': '',
      'md5sum': 'b2d11888adb1c45a0ad3efb54e21d6d4',
      'size': 2238892,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/textnorm_kestrel.cfg',
      'url': '',
      'md5sum': '97427f37e374112ac84c311c866c33b9',
      'size': 564,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/phonology.cfg',
      'url': '',
      'md5sum': 'b1c015977d245b6df2b734a3e20b0f8c',
      'size': 12198,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/phonology.pb',
      'url': '',
      'md5sum': 'ca9438857cdea216f38c0b9d53315cb2',
      'size': 2893,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/resolve_homographs_simple_nl.pb',
      'url': '',
      'md5sum': '1e83fe3653b364155c9de0b692f70360',
      'size': 2224,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/rewrite_android.far',
      'url': '',
      'md5sum': 'b39717398cd80366d46109ff2ad4dbfe',
      'size': 395696,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/roman_numeral_contexts.pb',
      'url': '',
      'md5sum': '26c3c2b952a288e956d977b3c8b3e960',
      'size': 8451,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/roman_numeral_android.far',
      'url': '',
      'md5sum': 'dc4e79c96ad125280b459e4047de614c',
      'size': 1130,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/textnorm_params.pb',
      'url': '',
      'md5sum': '715023773bd80c6f7ad15bb9aa226df9',
      'size': 16,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/tokenize_and_classify_android.far',
      'url': '',
      'md5sum': '171b1670e34d74311b9f11819009a65e',
      'size': 1038521,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/verbalize_android.far',
      'url': '',
      'md5sum': '82296c87e8152e7ad0d33f8b36bb7fae',
      'size': 380340,
    },
    {
      'path': '/voice_data_hmm_nl-NL_2/project',
      'url': '',
      'md5sum': '8dab9ead9efc875569715f2daf9c712e',
      'size': 1665,
    },
  ],
});
